#!/usr/bin/env python3

import argparse
import os
import pynckernel
import random
import struct
import sys
from sched import scheduler
from itertools import repeat
import copy

import multiprocessing 

class PathAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string):
        if self.dest == "path":
            path = argparse.Namespace()
            namespace.path.append(path)
        else:
            path = namespace.path[-1]
        setattr(path, self.dest, values)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", action=PathAction, default=[],
                        help="Add a path to the simulation")
    parser.add_argument("--loss", type=float, action=PathAction,
                        help="Loss probability of the path")
    parser.add_argument("--delay", type=float, action=PathAction,
                        help="Inter-packet delay of the path")
    parser.add_argument("--latency", type=float, action=PathAction,
                        help="Latency of a packet sent on the path")

    parser.add_argument("--source-size", type=int, default=1500,
                        help="Size of the source packets")
    parser.add_argument("--source-burst", type=int, default=1,
                        help="Number of consecutive source packets")
    parser.add_argument("--source-delay", type=float, default=1,
                        help="Inter-packet delay of the source")
    parser.add_argument("--packets", type=int, default=1000,
                        help="Number of packets to send")
                        
    parser.add_argument("--coder", nargs=2, metavar=["NAME", "VALUE"], action="append", default=[],
                        help="Set an option for all coders")
    parser.add_argument("--encoder", nargs=2, metavar=["NAME", "VALUE"], action="append", default=[],
                        help="Set an option for the encoder")
    parser.add_argument("--decoder", nargs=2, metavar=["NAME", "VALUE"], action="append", default=[],
                        help="Set an option for the decoder")
    parser.add_argument("--seed", type=int, default=0,
                        help="Seed for the random number generator")

    parser.add_argument("--encoder-pcap", type=str, default=None,
                        help="Write packets sent and received by the encoder to the file")
    parser.add_argument("--decoder-pcap", type=str, default=None,
                        help="Write packets sent and received by the decoder to the file")

    parser.add_argument("--no-timers", default=False, action="store_true",
                        help="Disable all protocol timers")
    parser.add_argument("--no-flood", default=False, action="store_true",
                        help="Disable over-filling of encoders")

    return parser.parse_args()

class Clock:
    def __init__(self):
        self.time = 0

    def get_time(self):
        return self.time

    def sleep(self, secs):
        self.time += secs


class Path:
    def __init__(self, path, *, latency=1, delay=1, loss=0):
        self.name = path
        self.rand = None
        self.sched = None
        self.latency = latency
        self.delay = delay
        self.loss = loss
        self.ready = False
        self.ready_callback = None

    def start(self, rand, sched):
        self.rand = rand
        self.sched = sched
        self.ready = True

    def send(self, callback, *args):
        if self.rand.random() >= self.loss:
            #scheduler.enter(delay, priority, action, argument=(), kwargs={})
            self.sched.enter(self.latency, 1, lambda: callback(*args))

        self.ready = False
        self.sched.enter(self.delay, 2, self.rearm)

    def rearm(self):
        self.ready = True
        if self.ready_callback:
            self.ready_callback()

    def on_ready(self, callback):
        self.ready_callback = callback


class Traffic:
    def __init__(self, *, burst, delay, size, packets):
        self.sched = None
        self.sender = None
        self.delay = delay
        self.burst = burst
        self.size = size
        self.packets = packets
        self.event = None

    def start(self, sched, sender):
        self.counter = 0
        self.sched = sched
        self.sender = sender
        self.event = self.sched.enter(0, 0, self.generate)

    def generate(self):
        for _ in range(self.burst):
            source = bytearray(os.urandom(self.size))
            self.sender(self.counter, source)
            self.counter += 1
            
        if self.counter < self.packets:
            self.event = self.sched.enter(self.delay, 0, self.generate)


def simulate(traffic, paths, encoder, decoder, seed=1, enc_capture=None, dec_capture=None, timers=True, flood=True):
    stats = []
    random.seed(seed)
    rand = random.Random(seed) # common random number generator
    clock = Clock() # simulated time
    sched = scheduler(clock.get_time, clock.sleep)

    cores=multiprocessing.cpu_count()
    pool=multiprocessing.Pool(processes=cores)
    
    decs=[]
    if timers:
        enc = pynckernel.Encoder(sched, encoder)
        for i in range(len(paths)):
            dec = pynckernel.Decoder(sched, decoder)
            decs.append(dec)
    else:
        enc = pynckernel.Encoder(None, encoder)
        for i in range(len(paths)):
            dec = pynckernel.Decoder(None, decoder)
            decs.append(dec)

    def send_source(no, source):
        
        assert(no == len(stats))
        
        # stats.append(dict(sent=clock.get_time()))
        stats.append(dict(sent='Source Sent'))
        # struct.pack_into(format, buffer, offset, v1, v2, ...)
        # Pack the values v1, v2, … according to the format string 
        # format and write the packed bytes into the writable buffer 
        # buffer starting at position offset. Note that offset is a required argument.
        struct.pack_into("!L", source, 0, no)

        if enc_capture:
            enc_capture(0, "source", clock.get_time(), source)

        if flood or not enc.full(): # enc collect as much as it can
            enc.put_source(source)

    def receive_coded(path_no, coded, offset, length):
        if dec_capture:
            dec_capture(path_no, "coded", clock.get_time(), coded[offset:offset+length])           
        
        # dec=decs[0]
        # decs.remove(dec)
        # decs.append(dec)
        # dec=decs[path_no]

        # dec.put_coded(coded, offset, length)
        decs[path_no].put_coded(coded,offset,length)

    def send_coded():

        ready_stats=[paths[i].ready for i in range(len(paths))]

        if enc.has_coded() and all(ready_stats):
            coded, offset, length = enc.get_coded()
            # print('offset:',offset,'length:',length)
                     
            for i in range(len(paths)):                       
                # cp_coded=copy.deepcopy(coded)
                path = paths[i]

                # path = paths[0]
                # paths.remove(path)
                # paths.append(path)
                
                if enc_capture:
                    enc_capture(i, "coded", clock.get_time(), coded[offset:offset+length])

                # path.send(receive_coded, i, cp_coded, offset, length)
                path.send(receive_coded,i, coded, offset, length)
                
         

    # def receive_source(dec):
    def receive_source(path_no):    
        while decs[path_no].has_source():
            
            # path_no=decs.index(dec)

            source, offset, length = decs[path_no].get_source()
            if dec_capture:
                dec_capture(0, "source", clock.get_time(), source[offset:offset+length])

            counter, = struct.unpack_from("!L", source, offset)
            
            path_name='path'+str(path_no) # have differenent delay numbers
            
            stats[counter][path_name] = 'arrived'

            # clock.get_time() - stats[counter]["sent"]

    #--------------------------------
    # hook up callbacks
    enc.on_coded_ready(send_coded) # try to send coded when a packet is ready

    def create_receiver(no):
        return lambda: receive_source(no)

    # for i in range(len(paths)):  
    #     decs[i].on_source_ready(lambda : receive_source(i)) # get decoded packets when available
    for i in range(len(paths)):
        decs[i].on_source_ready(create_receiver(i))

    for p in paths:
        p.on_ready(send_coded) # try to send coded when a path is ready
    #----------------------------------

    # start network
    for p in paths:
        p.start(rand, sched)

    # start traffic model
    traffic.start(sched, send_source)

    # run simulation
    sched.run()

    return stats

class Capture:
    def __init__(self, path):
        import dpkt

        self.path = path
        self.file = open(path, "wb")
        self.writer = dpkt.pcap.Writer(self.file)
        self.eth = dpkt.ethernet.Ethernet
        self.ip = dpkt.ip.IP
        self.udp = dpkt.udp.UDP

    def __call__(self, path, type, time, payload):
        path += 1
        if type == "coded":
            path += 1

        ethsrc = bytes([0, 0x42, 0, 0, 1, path])
        ethdst = bytes([0, 0x42, 0, 0, 2, path])
        ipsrc = bytes([192, 168, 1, path])
        ipdst = bytes([192, 168, 2, path])

        udp = self.udp(sport=50001, dport=50001, ulen=len(payload)+8, data=payload)
        ip = self.ip(src=ipsrc, dst=ipdst, p=0x11, data=udp)
        eth = self.eth(src=ethsrc, dst=ethdst, type=0x0800, data=ip)
        self.writer.writepkt(eth.pack(), time)

def main(args):
    traffic = Traffic(packets=args.packets,
                      delay=args.source_delay,
                      burst=args.source_burst,
                      size=args.source_size)

    coder = dict(symbol_size=args.source_size)
    coder.update({opt[0]: opt[1] for opt in args.coder})

    decoder = {}
    decoder.update(coder)
    decoder.update({opt[0]: opt[1] for opt in args.decoder})
    
    encoder = {}
    encoder.update(coder)
    encoder.update({opt[0]: opt[1] for opt in args.encoder})

    # paths = [Path(**p.__dict__) for p in args.path]
    paths=[]
    path1=Path(path="Vehicle1", # Path(self, path, *, latency=1, delay=1, loss=0) 
               loss=0.3) 

    path2=Path(path="Vehicle2",
               loss=0.5)
    path3=Path("Vehicle3",loss=0)
    
    paths.append(path1)
    paths.append(path2)
    paths.append(path3)

    enc_capture = None
    if args.encoder_pcap:
        enc_capture = Capture(args.encoder_pcap)

    dec_capture = None
    if args.decoder_pcap:
        dec_capture = Capture(args.decoder_pcap)

    stats = simulate(traffic, paths, encoder, decoder, args.seed, 
                enc_capture, dec_capture, not args.no_timers, not args.no_flood)

    for i in range(len(stats)):
        sentence=str(i)+' | '+stats[i]['sent']+' | '
        for j in range(len(paths)):
            # print(i, stats[i]["sent"], stats[i]["sent"],stats[i].get("delay"+str(j), "nan"))
            sentence +=stats[i].get("path"+str(j), "loss")+' '
        print(sentence)

    sum_result_paths=[]
    for j in range(len(paths)):

        # print(stats[i])
        sum_result=0        

        for i in range(len(stats)):
            if stats[i].get('path'+str(j),'loss') == 'arrived':
                sum_result +=1

        sum_result_paths.append(sum_result)
    
    for i in range(len(paths)):
        print('path%d: %d/%d' %(i,sum_result_paths[i], len(stats)))

if __name__ == "__main__":
    args = parse_args()
    main(args)
